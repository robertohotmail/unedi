<?php
include 'header.php';
include 'conexion.php';
include 'parameters.php';
require('conexion.php');
session_start();
if(isset($_SESSION["usuario"])){
  header("location: index.php");
}
?>
 <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
  <script src="js/responsiveslides.min.js"></script>
<script> 
    $(function () {
      $("#slider").responsiveSlides({
        auto: true,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
  </script>  
<div class="banner">    
   <div class="header">
       <div class="logo">
         <a href="home.php"><img src="images/LOGO.png" alt=""/></a>
       </div>
       <?php include 'navbaradmin.php'?>
        <!-- script-for-menu -->
     <script>
        $("span.menu").click(function(){
          $("ul.navig").slideToggle("slow" , function(){
          });
        });
     </script>
   </div>
     <!-- ESTA SECCIONN ES LA DEL SLIDER EN MOVIMIENTO--> 
     </div>
    <br><br><br><br><br><br>

<?php

$title = "Editar cuadro";
/* Llamar la Cadena de Conexion*/


$id = (int) $_GET['id']; 
$sql = mysqli_query($conexion, "select * from noticia where id_noticia='$id' limit 0,1");
$count = mysqli_num_rows($sql);
if ($count == 0) {
  //header("location: bannerlist.php");
  //exit;
} 
$rw = mysqli_fetch_array($sql);
$titulo = $rw['titulo'];
$descripcion = $rw['descripcion'];

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  echo $id;
  $titulo = $_POST['titulo'];
  $descripcion = $_POST['descripcion'];

  $query = "UPDATE noticia set titulo='$titulo',descripcion='$descripcion' WHERE id_noticia='$id'";
  mysqli_query($conexion, $query);
  echo '<script type="text/javascript">
           window.location = "http://localhost/unedi/adminNoticias.php"
      </script>';
}

?>
<div class="container">
<div class="blog-head">
        <h2><font color="F4F6F7">Modifica las noticias</font></h2>
     
			</div>
    
<div style="color: blue;">

  <p>
  <span style="color: red;">Aquí puedes editar y subir una nueva imagen para su galería</span> 
  </p>
</div>
</div>

<div class="container">
  
<div class="container">
  <form action="actualizarNoti.php?id=<?php echo $_GET['id']; ?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="titulo" class="col-sm-1 control-label">Titulo</label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="titulo" value="<?php echo $titulo; ?>" required name="titulo">
      </div>
    </div>
    </div>
    
    <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-1 control-label">Descripción</label>
      <div class="col-sm-7">
        <textarea class='form-control' name="descripcion" id="descripcion" required rows=8><?php echo $descripcion; ?></textarea>
      </div>
      </div>
      </div>


      <div class="container">
      <div class="form-group">
      <div class="col-sm-8">
              <input type="file" class="form-control" name="txtFoto" id="txtFoto"><br>
            </div>
            </div>
      
    </div>
    
    <div class="form-group">
      <div id='loader'></div>
      <div class='outer_div'></div>
      <div class=" col-sm-7">
        <button type="submit" class="btn btn-success" name="update">Actualizar datos</button>
        <br><br><br>
      </div>
    </div>
  </form>
  </div>
        <?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>
