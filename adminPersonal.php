<?php
include('conexion.php');
include('logoAdmin.php');
?>
<body>  
<div class="container">
<!-- Boton para accionar modal -->
<center>
<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalFormEst">
    <i class="fa fa-user-circle-o" aria-hidden="true"></i> Agregar Personal
</button>
</center><br><br><br>
    <table class="table table-bordered"  id="tablaDatos">
        <thead>
            <tr>
                <th class="text-center">Nro</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Correo</th>
                <th>Imagen</th>
                <th>Cargo</th>
                <th>Ingreso a la UNEDI</th>
                <th>Labores</th>
                <th class="text-center">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $consulta = "SELECT * from personal where eliminado ='n'";
            $resultado = mysqli_query($conexion, $consulta);
                while ($row = mysqli_fetch_array($resultado)) {
                  $id=$row[0];
                  echo "
                  <tr>
                    <td>".$id."</td>
                    <td>".$row['nombres']."</td>
                    <td>".$row['apellidos']."</td>
                    <td>".$row['correo']."</td>
                    <td><img src='uploads/" . $row['imagen'] . "' width='75%'></td>
                    <td>".$row['cargo']."</td>
                    <td>".$row['fechaIngresoInstitucional']."</td>
                    <td>".$row['labores']."</td>
                    <td>
                    ";
                    echo '
                    <a class="btn btn-warning" href="editarPersonal.php?id='.$id.'">Actualizar
                    </a>
                    <a class="btn btn-danger" href="elimarPersonal.php?id='.$id.'">Eliminar</a></td>
                  </tr>';
          }
          ?>
    </table>
</div>
<!-- Modal del formulario -->
<div class="modal fade" id="modalFormEst" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <!-- Header del Modal del formulario --> 
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <center>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Agregar Personal</h4>
                </center>
            </div>
            <!-- Cuerpo del Modal del formulario -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" action="addPersonal.php" enctype="multipart/form-data" method="post">
               <div class="form-group">
                <label class="form-control text-center" for="txtNombre">Nombres :</label>
               <input type="text" class="form-control" name="txtNombre" id="txtNombre" placeholder="Ej. Juan José" required=""><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtApellidos">Apellidos :</label>
               <input type="text" class="form-control" name="txtApellidos" id="txtApellidos" placeholder="Ej. Intriago Villarreal" required=""><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtTitulo">Titulo :</label>
               <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Ej. Juan José" required=""><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtCorreo">Correo :</label>
               <input type="text" class="form-control" name="txtCorreo" id="txtCorreo" placeholder="Ej. juanintriago@prueba.com" required=""><br>
             </div>

             <div class="form-group">
              <label class="form-control text-center" for="txtFoto">Fotografía :</label>
              <input type="file" class="form-control" name="txtFoto" id="txtFoto"><br>
            </div>

             <div class="form-group">
                <label class="form-control text-center" for="txtCargo">Cargo :</label>
               <select class="form-control" name="txtCargo" id="txtCargo" required="">
                <option>--Seleccione su Cargo--</option>
                 <?php
                 $query = $conexion -> query ("SELECT * FROM cargo where eliminado='n'");
                  while ($valores = mysqli_fetch_array($query)) { 
                  echo '<option value="'.utf8_encode($valores[descripcion]).'">'.utf8_encode($valores[descripcion]).'</option>';
                  }?>
                  <option>--Fin de la Lista--</option>
               </select> <br>
             </div>


  

             <div class="form-group">
                <label class="form-control text-center" for="txtFecha">Fecha de Ingreso a la UNEDI :</label>
               <input type="date" class="form-control" name="txtFecha" id="txtFecha" >
               <br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtLabor">Labores que Realiza :</label>
               <textarea class="form-control text-center" name="txtLabor" required placeholder="Coordinación y dirección de la institución."></textarea>
               <br>
             </div>
            <input class="form-control btn btn-primary" type="submit" value="Guardar">
            </form>
          </div>
          </div>
        </div>
      </div>
<?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>