-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-03-2020 a las 16:29:28
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bddunedi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `url_image` varchar(255) NOT NULL,
  `estado` int(1) NOT NULL,
  `orden` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banner`
--

INSERT INTO `banner` (`id`, `titulo`, `descripcion`, `url_image`, `estado`, `orden`) VALUES
(48, 'Titulo1', 'ejemplo', '10.jpg', 1, 1),
(49, 'titulo 2', 'prueba2', 'img13.jpg', 1, 2),
(50, 'titulo3', 'pruba3', 'Captura de pantalla (13).png', 1, 3),
(51, 'titulo 4', 'orueba4', 'fondo.jpg', 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id_labor` int(11) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id_labor`, `descripcion`, `estado`, `eliminado`) VALUES
(1, 'Rector(a)', 'a', 'n'),
(2, 'Primer delegado(a) del Consejo Ejecutivo', 'a', 'n'),
(3, 'Segundo Delegado(a) del Consejo Ejecutivo', 'a', 'n'),
(4, 'Representante de los Padres y Madres de Familia', 'a', 'n'),
(5, 'Presidente(a) del Consejo Estudiantil', 'a', 'n'),
(6, 'Secretario(a) General', 'a', 'n'),
(7, 'ViceRector(a)', 'a', 'n'),
(8, 'Inspectora General', 'a', 'n'),
(9, 'Secretaria', 'a', 'n'),
(10, 'Psicóloga Educativa', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorianoticia`
--

CREATE TABLE `categorianoticia` (
  `id_categoriaNoticia` int(11) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorianoticia`
--

INSERT INTO `categorianoticia` (`id_categoriaNoticia`, `descripcion`, `estado`, `eliminado`) VALUES
(1, 'Matriculas', 'a', 'n'),
(2, 'Social', 'a', 'n'),
(3, 'Educativa', 'a', 'n'),
(4, 'Agenda', 'a', 'n'),
(5, 'Instalaciones', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriausuario`
--

CREATE TABLE `categoriausuario` (
  `id_categoriaUsuario` int(11) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoriausuario`
--

INSERT INTO `categoriausuario` (`id_categoriaUsuario`, `descripcion`, `estado`, `eliminado`) VALUES
(1, 'Administrador', 'a', 'n'),
(2, 'Docente', 'a', 'n'),
(3, 'Estudiante', 'a', 'n'),
(4, 'superUsuario', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuadrohonor`
--

CREATE TABLE `cuadrohonor` (
  `id_estudiante` int(11) NOT NULL,
  `nombres` varchar(250) NOT NULL,
  `apellidos` varchar(250) NOT NULL,
  `dignidad` varchar(250) NOT NULL,
  `promedio` float(8,3) NOT NULL,
  `imagen` varchar(250) NOT NULL DEFAULT 'bandera.png',
  `descripcion` varchar(250) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuadrohonor`
--

INSERT INTO `cuadrohonor` (`id_estudiante`, `nombres`, `apellidos`, `dignidad`, `promedio`, `imagen`, `descripcion`, `estado`, `eliminado`) VALUES
(1, 'Erika Mariuxi', 'Ruales Pantoja', 'Abanderado del Pabellon Nacion', 9.435, '	bandera.png', 'Soy una estudiante muy cumplida y mi objetivo es salir delante de la mejor manera', 'a', 'n'),
(2, 'Anda Lucia', 'Lopez Ayala', 'Portaestandarte de la Ciudad', 9.225, 'bandera.png', 'Soy muy puntual', 'a', 'n'),
(3, 'Jonathan Rodrigo', 'Obando Sanchez', 'Portaestandarte del Plantel', 9.006, 'wL98EK.jpg', 'Soy un estudiante responsable', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dignidadesbandera`
--

CREATE TABLE `dignidadesbandera` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dignidadesbandera`
--

INSERT INTO `dignidadesbandera` (`id`, `descripcion`, `estado`, `eliminado`) VALUES
(1, 'Abanderado del Pabellon Nacion', 'a', 'n'),
(2, 'Portaestandarte de la Ciudad', 'a', 'n'),
(3, 'Abanderado/a del Pabellón Nacional', 'a', 'n'),
(4, 'Porta Estandarte de la Cuidad', 'a', 'n'),
(5, 'Portaestandarte del Plantel', 'a', 'n'),
(6, '1er. Escolta del Pabellón Nacional', 'a', 'n'),
(7, '2do. Escolta del Pabellón Nacional', 'a', 'n'),
(8, '1er. Escolta del Pabellón de La Ciudad', 'a', 'n'),
(9, '2do. Escolta del Pabellón de La Ciudad', 'a', 'n'),
(10, '1er. Escolta del Pabellón del Plantel', 'a', 'n'),
(11, '2do. Escolta del Pabellón del Plantel', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `color` varchar(7) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `title`, `color`, `start`, `end`, `estado`, `eliminado`) VALUES
(1, 'Evento Azul', '#0071c5', '2017-08-01 00:00:00', '2017-08-02 00:00:00', 'a', 'n'),
(2, 'Eventos 2', '#40E0D0', '2017-08-02 00:00:00', '2017-08-03 00:00:00', 'a', 'n'),
(3, 'Doble click para editar evento', '#008000', '2017-08-03 00:00:00', '2017-08-07 00:00:00', 'a', 'n'),
(4, 'evento de prueba', '#FFD700', '2019-12-30 00:00:00', '2019-12-31 00:00:00', 'a', 'n'),
(5, 'Juramento de Bandera', '#40E0D0', '2019-12-30 07:15:00', '2019-12-30 11:50:00', 'a', 'n'),
(6, 'Juegos deportivos Equipo 1 vs Equipo 2', '#0071c5', '2020-01-02 10:30:00', '2020-01-03 11:25:00', 'a', 'n'),
(7, 'Evento prueba 2', '#FF0000', '2020-01-01 08:00:00', '2020-01-01 12:00:00', 'a', 'n'),
(11, 'Evento nuevo', '#FF0000', '2020-01-28 00:00:00', '2020-01-29 00:00:00', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `image`
--

INSERT INTO `image` (`id`, `title`, `folder`, `src`, `created_at`) VALUES
(5, 'Etapa 2', 'uploads/', '4.jpg', '2020-02-18 10:11:29'),
(6, 'Etapa3', 'uploads/', '5.jpg', '2020-02-18 10:11:42'),
(7, 'Etapa 4', 'uploads/', '10.jpg', '2020-02-18 10:19:31'),
(8, 'Etapa 5', 'uploads/', '9.jpg', '2020-02-18 10:19:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informacioninstitucional`
--

CREATE TABLE `informacioninstitucional` (
  `id_dato` int(11) NOT NULL,
  `nombreInstitucion` varchar(250) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `correoInsitucion` varchar(500) NOT NULL,
  `sitioWeb` varchar(250) NOT NULL,
  `mision` varchar(1000) NOT NULL,
  `vision` varchar(1000) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n',
  `direccionIntitucion` varchar(250) NOT NULL,
  `periodoAcademico` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informacioninstitucional`
--

INSERT INTO `informacioninstitucional` (`id_dato`, `nombreInstitucion`, `telefono`, `correoInsitucion`, `sitioWeb`, `mision`, `vision`, `estado`, `eliminado`, `direccionIntitucion`, `periodoAcademico`) VALUES
(1, 'UNIDAD EDUCATIVA IMBABURA PCEI', '062652847 - 0989403966', 'unedi.edu.ec', 'http://www.unedi.edu.ec/blog/', 'La UNEDI es una instituciï¿½n de Educaciï¿½n alternativa para los sectores marginados de la educaciï¿½n regular presencial, que tiene como misiï¿½n esencial contribuir a la formaciï¿½n integral de bachilleres en ciencias y tï¿½cnicos con excelente calidad acadï¿½mica, y valores humanos, ï¿½ticos, cristianos y cï¿½vicos, capaces de continuar los estudios, insertarse y/o mejorar su desempeï¿½o en el mercado ocupacional y ser entre activo en el desarrollo de la comunidad.', 'La Unidad Educativa Imbabura se propone ser una Instituciï¿½n Educativa con excelencia en los procesos acadï¿½micos y administrativos; actualizada tecnolï¿½gicamente, personal altamente calificado e infraestructura fï¿½sica propia, destinada a lograr el desarrollo de participantes con alto grado de valores humanos, cï¿½vicos, morales y cristianos. ', 'a', 'n', 'PLANTA CENTRAL: Av. El Retorno 2597 y Nazacota ', '2019-2020');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE `noticia` (
  `id_noticia` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descripcion` varchar(1000) NOT NULL,
  `fecha` varchar(100) NOT NULL,
  `imagen` varchar(20) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id_noticia`, `titulo`, `descripcion`, `fecha`, `imagen`, `categoria`, `estado`, `eliminado`) VALUES
(5, 'Sala de maestros', 'Dentro de este campus  se realizan reuniones, planificaciones para educadores, y administrativos, cuenta con una gran mesa de reuniones, cafetera y agua disponible todo el tiempo.', '17/01/2020', '20181019_175813.jpg', 'Campus', 'a', 'n'),
(6, 'Canchas Deportivas', 'Aqui se realizan multiples actividades deportivas par cada uno de los estudiantes de la institucion.', '19/01/2020', '20180929_105433.jpg', 'Campus', 'a', 'n'),
(12, 'Noticia de prueba', 'Juramento a la bandera', '04/02/2020', '4.jpg', 'Matriculas', 'a', 'n'),
(14, 'Consorcio de Docentes', 'OrganizaciÃ³n de los docentes', '17/02/2020', '10.jpg', 'Educativa', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oferta`
--

CREATE TABLE `oferta` (
  `id_noticia` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descripcion` varchar(1000) NOT NULL,
  `fecha` varchar(100) NOT NULL,
  `imagen` varchar(20) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oferta`
--

INSERT INTO `oferta` (`id_noticia`, `titulo`, `descripcion`, `fecha`, `imagen`, `eliminado`) VALUES
(16, '7777', '777', '05/03/2020', '1.png', 'n'),
(17, 'asdasd', 'asdsad', '05/03/2020', '4.png', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `id_personal` int(11) NOT NULL,
  `titulo` varchar(20) NOT NULL,
  `nombres` varchar(250) NOT NULL,
  `apellidos` varchar(250) NOT NULL,
  `correo` varchar(250) NOT NULL,
  `cargo` varchar(250) NOT NULL,
  `imagen` varchar(250) NOT NULL,
  `fechaIngresoInstitucional` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `labores` varchar(250) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`id_personal`, `titulo`, `nombres`, `apellidos`, `correo`, `cargo`, `imagen`, `fechaIngresoInstitucional`, `labores`, `estado`, `eliminado`) VALUES
(1, 'Lic.', 'Marco', 'BuitrÃ³n', 'correo@uniandes.edu.ec', 'Rector(a)', 'profesor.jpg', '0000-00-00 00:00:00', 'Dirije en su totalidad a la instituciÃ³n', 'a', 'n'),
(2, 'werwer', 'Luis', 'werwer', 'werwe', 'Primer delegado(a) del Consejo Ejecutivo', 'profesor2.jpg', '0000-00-00 00:00:00', 'werwe', 'a', 'n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombreUsuario` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `categoriaUsuario` varchar(150) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'a',
  `eliminado` char(1) NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombreUsuario`, `password`, `categoriaUsuario`, `estado`, `eliminado`) VALUES
(1, 'juanIntriago', '2fcc75388ae241e1d5f3b7fd0d7d4966706dc4ce', 'superUsuario', 'a', 'n'),
(2, 'robertoFernandez', '281067323acfac001591a4e4914c5ed99fd101e5', 'superUsuario', 'a', 'n');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id_labor`);

--
-- Indices de la tabla `categorianoticia`
--
ALTER TABLE `categorianoticia`
  ADD PRIMARY KEY (`id_categoriaNoticia`);

--
-- Indices de la tabla `categoriausuario`
--
ALTER TABLE `categoriausuario`
  ADD PRIMARY KEY (`id_categoriaUsuario`);

--
-- Indices de la tabla `cuadrohonor`
--
ALTER TABLE `cuadrohonor`
  ADD PRIMARY KEY (`id_estudiante`);

--
-- Indices de la tabla `dignidadesbandera`
--
ALTER TABLE `dignidadesbandera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `informacioninstitucional`
--
ALTER TABLE `informacioninstitucional`
  ADD PRIMARY KEY (`id_dato`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`id_noticia`);

--
-- Indices de la tabla `oferta`
--
ALTER TABLE `oferta`
  ADD PRIMARY KEY (`id_noticia`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`id_personal`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id_labor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `categorianoticia`
--
ALTER TABLE `categorianoticia`
  MODIFY `id_categoriaNoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `categoriausuario`
--
ALTER TABLE `categoriausuario`
  MODIFY `id_categoriaUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `cuadrohonor`
--
ALTER TABLE `cuadrohonor`
  MODIFY `id_estudiante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `dignidadesbandera`
--
ALTER TABLE `dignidadesbandera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `informacioninstitucional`
--
ALTER TABLE `informacioninstitucional`
  MODIFY `id_dato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `oferta`
--
ALTER TABLE `oferta`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `id_personal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
