<?php 
include 'header.php';
include 'conexion.php';
include 'parameters.php';
require('conexion.php');
session_start();
if(isset($_SESSION["usuario"])){
	header("location: home.php");
}
if(!empty($_POST)){
	$usuario = mysqli_real_escape_string($conexion,$_POST['txtUsuario']);
	$password = mysqli_real_escape_string($conexion,$_POST['txtPassword']);
	$sha1_pass = sha1($password);
	$consulta = "SELECT id_usuario, categoriaUsuario FROM usuario WHERE nombreUsuario = '$usuario' AND password ='$sha1_pass'";
	$result=$conexion->query($consulta);
	$filas = $result->num_rows;

	if($filas > 0){
		$filas = $result->fetch_assoc();
		$_SESSION['id_session_usuario'] = $filas['id_usuario'];
		$_SESSION['usuario'] = $filas['nombreUsuario'];
		$_SESSION['tipo_usuario'] = $filas['categoriaUsuario'];
		header("location: home.php");
	}else{
		$error = "Los datos son incorrectos, intente verificando lo escrito.!";
	}
}
?>
 <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
<!-- banner -->
<script src="js/responsiveslides.min.js"></script>
<script> 
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	nav: true,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
  </script>  
<div class="banner">	  
	 <div class="header">
			 <div class="logo">
				 <a href="index.php"><img src="images/LOGO.png" alt=""/></a>
			 </div>
			 <?php include 'navbar.php'?>
			  <!-- script-for-menu -->
		 <script>
				$("span.menu").click(function(){
					$("ul.navig").slideToggle("slow" , function(){
					});
				});
		 </script>
	 </div>
		 <!-- ESTA SECCIONN ES LA DEL SLIDER EN MOVIMIENTO--> 
		 </div>
		 <br><br><br><br><br><br><br>
		 <div>
		  <tr/>
		  <center><iframe src="carousel.php"  style="height:300px;width:720px;"></iframe></center>
		</div>



		
<!-- blog-page -->
<div class="blog">
		<div class="container">
			<div class="blog-head">
				<h2><b><font color="F4F6F7">Cartelera de Noticias</font> </b></h2>
			</div>
			<div class="col-md-8 blog-left">
				<?php
                 		$query = $conexion -> query ("SELECT * FROM noticia WHERE eliminado ='n' and categoria<>'Campus' LIMIT 5");
                  		while ($valores = mysqli_fetch_array($query)) {
  						echo '<div class="blog-info">
  						<h3 <b class="text-alert">'.$valores['titulo'].'</b></h3>
  						<h4 class="text-second">Publicada en : '.$valores['fecha'].' - - Categoría : '.$valores['categoria'].'</h4>
  						<div class="blog-info-text">
  						<div class="blog-img">
							<a href="#"> <img src="uploads/'.$valores['imagen'].'" class="img-responsive zoom-img" alt=""/></a>
						</div>
						<h5 class="text-warning">'.$valores['descripcion'].'</h5>
						</div>
						</div><br>
						<hr width=400>
  						';
                  		}?>
                  			
                  		</div>	
			<!--<div class="col-md-4 single-page-right">
				<div class="category blog-ctgry">
					<h4>Categorias</h4>
					<div class="list-group">
						<?php
            //$query = $conexion -> query ("SELECT * FROM categorianoticia WHERE eliminado ='n'");
                  		//while ($valores = mysqli_fetch_array($query)) {
  						//echo '
  						//<a href="#" class="list-group-item">'.$valores['descripcion'].'</a>
  						//';
                  		//}?>
					</div>
				</div>			
			</div>-->
			<div class="clearfix"> </div>
		</div>	
	</div>	
	<!--//blog-->
<!-- Modal del formulario -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <!-- Header del Modal del formulario --> 
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <h3 class="modal-title" id="myModalLabel"><i class="fa fa-terminal" aria-hidden="true"></i> Ingresar como Administrador</h3>
            </div>
            <!-- Cuerpo del Modal del formulario -->
            <div class="modal-body">            	
                <p class="statusMsg"></p>
                <form role="form" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
                	<center>
                		<img class="img-responsive pic-center" src="img/unedi.png" width="50%">
                	</center>                	
               <div class="form-group">
                <label class="form-control text-center" for="txtUsuario">Usuario:</label>
               <input type="text" class="form-control" name="txtUsuario" id="txtUsuario" placeholder="Ej. consueloHernandez"><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtPassword">Password:</label>
                <input type="password" class="form-control" name="txtPassword" id="txtPassword"placeholder="Digite aqui su password y no la olvide"><br>
             </div>
            <input class="form-control btn btn-primary" type="submit" value="Acceder">            
            </form>
          </div>            
          </div> 
        </div>
      </div>
<?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>