<?php
include('conexion.php');
include('logoAdmin.php');
?>

<body>
  <script type="text/javascript">
    function validar(campo) {
      var elcampo = document.getElementById(campo);
      if ((!validarNumero(elcampo.value)) || (elcampo.value == "")) {
        elcampo.value = "";
        elcampo.focus();
        document.getElementById('mensaje').innerHTML = '<h4 class="text-danger">Debe ingresar un número</h4>';
      } else {
        document.getElementById('mensaje').innerHTML = '<h4 class="text-success">Si es un numero</h4>';
      }
    }

    function validarNumero(input) {
      return (!isNaN(input) && parseInt(input) == input) || (!isNaN(input) && parseFloat(input) == input);
    }
  </script>

  <div class="container">
    <!-- Boton para accionar modal -->
    <center>
      <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalFormEst">
        <i class="fa fa-graduation-cap" aria-hidden="true"></i> Agregar Estudiante
      </button>
    </center><br><br><br>
    <table class="table table-bordered" id="tablaDatos">
      <thead>
        <tr>
          <th class="text-center">Nro</th>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Dignidad</th>
          <th>Promedio</th>
          <th>Imagen</th>
          <th>Descripcion</th>
          <th class="text-center">Opciones</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $consulta = "SELECT * from cuadrohonor where eliminado ='n'";
        $resultado = mysqli_query($conexion, $consulta);
        while ($row = mysqli_fetch_array($resultado)) {
          $id = $row[0];
          echo "
                  <tr>
                    <td>" . $id . "</td>
                    <td>" . $row['nombres'] . "</td>
                    <td>" . $row['apellidos'] . "</td>
                    <td>" . $row['dignidad'] . "</td>
                    <td>" . $row['promedio'] . "</td>
                    <td><img src='uploads/" . $row['imagen'] . "' width='25%'></td>
                    <td>" . $row['descripcion'] . "</td>
                    <td>
                    ";
          echo '
                    <a class="btn btn-warning" href="EditarAdminCuadro.php?id=' . $id . '">Actualizar
                    </a>
                    <a class="btn btn-danger" href="elimarCuadro.php?id=' . $id . '">Eliminar</a></td>
                  </tr>';
        }
        ?>
    </table>
  </div>
  <!-- Modal del formulario -->
  <div class="modal fade" id="modalFormEst" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Header del Modal del formulario -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Cerrar</span>
          </button>
          <center>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Agregar Estudiante</h4>
          </center>
        </div>
        <!-- Cuerpo del Modal del formulario -->
        <div class="modal-body">
          <p class="statusMsg"></p>
          <form role="form" action="addCuadro.php" enctype="multipart/form-data" method="post">
            <div class="form-group">
              <label class="form-control text-center" for="txtNombre">Nombre :</label>
              <input type="text" class="form-control" name="txtNombre" id="txtNombre" placeholder="Ej. Juan José" required=""><br>
            </div>
            <div class="form-group">
              <label class="form-control text-center" for="txtApellidos">Apellidos :</label>
              <input type="text" class="form-control" name="txtApellidos" id="txtApellidos" placeholder="Ej. Intriago Villarreal" required=""><br>
            </div>
            <div class="form-group">
              <label class="form-control text-center" for="txtDignidad">Dignidad:</label>
              <select class="form-control" name="txtDignidad" id="txtDignidad" required="">
                <option>--Seleccione su Dignidad--</option>
                <?php
                $query = $conexion->query("SELECT * FROM dignidadesbandera");
                while ($valores = mysqli_fetch_array($query)) {
                  echo '<option value="' . utf8_encode($valores[descripcion]) . '">' . utf8_encode($valores[descripcion]) . '</option>';
                } ?>
                <option>--Fin de la Lista--</option>
              </select> <br>
            </div>
            <div class="form-group">
              <label class="form-control text-center" for="txtPromedio">Promedio :</label>
              <input type="text" class="form-control" name="txtPromedio" onkeyup="validar(this.id);" id="txtPromedio">
              <div id="mensaje"></div>
              <br>
            </div>
            <div class="form-group">
              <label class="form-control text-center" for="txtFoto">Fotografía :</label>
              <input type="file" class="form-control" name="txtFoto" id="txtFoto"><br>
            </div>
            <div class="form-group">
              <label class="form-control text-center" for="txtDescript">Reseña sobre el Estudiante :</label>
              <textarea class="form-control text-center" name="txtDescript" required placeholder="Soy una persona muy activa y colaboradora y mi objetivo es la excelencia para mi y mi institución."></textarea>
              <br>
            </div>
            <input class="form-control btn btn-primary" type="submit" value="Guardar">
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>
