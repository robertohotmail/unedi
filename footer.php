<!---->
<?php
include('parameters.php');
?>
<div class="footer">
	 <div class="container">
		 <div class="ftr-sec">
			 <div class="col-md-4 ftr-grid">
				 <h3><font color="58ACF2">Más sobre Nosotros :</font></h3>
				 <font color="white">Dirección</font> :<p><?php echo $dir;?></p><br>
			 </div>
			 <div class="col-md-4 ftr-grid">
				 <font color="white">Teléfono :</font><p><?php echo $telef;?></p>
				 <font color="white">E-mail :</font><p><?php echo $correo;?></p><br>
			 </div>
			 <div class="col-md-4 ftr-grid">
				 <font color="white">Sitio-Web :</font><p><?php echo $sitioW;?></p>
				 <font color="white">Periódo Académico :</font><p><?php echo $periodo;?></p>
			 </div>			
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<!---->
<div class="copywrite">
	 <div class="container">
		 <p>Copyright © 2019 UNEDI. </p>
	 </div>
</div>
<!---->
</body>
</html>