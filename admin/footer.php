<!---->
<?php
include('../parameters.php');
?>
<div class="footer">
	 <div class="container">
		 <div class="ftr-sec">
			 <div class="col-md-4 ftr-grid">
				 <h3>Más sobre Nosotros :</h3>
				 Dirección :<p><?php echo $dir;?></p><br>
			 </div>
			 <div class="col-md-4 ftr-grid">
				 Teléfono :<p><?php echo $telef;?></p>
				 E-mail :<p><?php echo $correo;?></p><br>
			 </div>
			 <div class="col-md-4 ftr-grid">
				 Sitio-Web :<p><?php echo $sitioW;?></p>
				 Periódo Académico :<p><?php echo $periodo;?></p>
			 </div>			
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<!---->
<div class="copywrite">
	 <div class="container">
		 <p>Copyright © 2019 UNEDI. </p>
	 </div>
</div>
<!---->
</body>
</html>