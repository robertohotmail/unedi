<!DOCTYPE HTML>
<html>
<head>
<title>UNEDI-Sitio-Web</title>
<link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="../css/font-awesome.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary JavaScript plugins) -->
<!-- Custom Theme files -->

<link href="../css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="../application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="../js/jquery.min.js"></script>
 <script src="../js/bootstrap.js"></script>

</head>
<?php
session_start();
if (!isset($_SESSION['id_session_usuario']))
    header("location:index.php");
?>
<!-- banner --> 
<div class="">	  
	 <div class="header">
			 <div class="logo">
				 <a href="home.php"><img src="../images/LOGO UNEDI.png" alt=""/></a>
			 </div>
			<?php include 'navbar.php'?>
			 <!-- script-for-menu -->
		 <script>
				$("span.menu").click(function(){
					$("ul.navig").slideToggle("slow" , function(){
					});
				});
		 </script>
		 <!-- script-for-menu -->
			 <div class="clearfix"></div>
	 </div>	  
</div>
<!---->
<?php
include "db.php";

$images = get_imgs();
?>
<html>
	<head>
		<title>Subir Multiples Imagenes - Evilnapsis</title>
		  <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">

	</head>
	<body>
	

		<div class="container">
			<div class="row">
				<div class="col-md-12">
			
				
	
		<h1>Imagenes</h1>
		
		<a href="./form.php" class="btn btn-default">Agregar Imagen</a> 
		<br><br>
		<?php if(count($images)>0):?>
				<table class="table table-bordered">
					<thead>
						<th>Imagen</th>
						<th>Texto a mostrar</th>
						<th>
					</thead>
			<?php foreach($images as $img):?>
				<tr>
				<td><img src="<?php echo $img->folder.$img->src; ?>" style="width:240px;">				</td>
				<td><?php echo $img->title; ?></td>
				<td>
				<a class="btn btn-success" href="./download.php?id=<?php echo $img->id; ?>">Descargar</a> 
				<a class="btn btn-danger" href="./delete.php?id=<?php echo $img->id; ?>">Eliminar</a>
			</td>
				</tr>
			<?php endforeach;?>
</table>
		<?php else:?>

			<h4 class="alert alert-warning">No hay imagenes!</h4>
		<?php endif; ?>
</div>
</div>
</div>
	</body>

</html>
