<div class="top-menu">
				 <span class="menu"></span>
				 <ul class="navig">
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							 <i class="fa fa-keyboard-o" aria-hidden="true"></i> 
					 		Admin
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					 		<a class="dropdown-item active" href="../adminUsuarios.php"><i class="fa fa-user-secret" aria-hidden="true"></i> Gestionar Administradores</a>
 							<a class="dropdown-item active" href="../logout.php"><i class="fa fa-power-off" aria-hidden="true"></i> Salir</a>
					 		</div>
					 	</div>
					 </li>
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							 <i class="fa fa-keyboard-o" aria-hidden="true"></i> 
					 		Slider
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					 		<a class="dropdown-item active" href="index.php"><i class="fa fa-user-secret" aria-hidden="true"></i> Gestionar Slider</a>
					 		</div>
					 	</div>
					 </li>
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-sticky-note-o" aria-hidden="true"></i> 
					 		Noticias
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  							<a class="dropdown-item active" href="../adminNoticias.php"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Administrar Noticias</a>
					 		</div>
					 	</div>
					 </li>
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-photo" aria-hidden="true"></i> 
					 		Galería
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  							<a class="dropdown-item active" href="../admin/bannerlist.php"><i class="fa fa-file-photo-o" aria-hidden="true"></i> Administrar Galería</a>
					 		</div>
					 	</div>
					 </li>
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-calendar-o" aria-hidden="true"></i>
					 		Eventos
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  							<a class="dropdown-item active" href="../adminEventos.php"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Administrar Eventos</a>
					 		</div>
					 	</div>
					 </li>
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-graduation-cap" aria-hidden="true"></i> 
					 		Cuadro Hon.
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  							<a class="dropdown-item active" href="../adminCuadro.php"><i class="fa fa-tasks" aria-hidden="true"></i> Administrar Cuadro</a>
					 		</div>
					 	</div>
					 </li>
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle-o" aria-hidden="true"></i> 
					 		Personal
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  							<a class="dropdown-item active" href="../adminPersonal.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Personal</a>
					 		</div>
					 	</div>
					 </li>
					 <li>
					 	<div class="dropdown">
					 	<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-university" aria-hidden="true"></i> 
					 		Institución
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  						<a class="dropdown-item active" href="../adminInstitucion.php"><i class="fa fa-edit" aria-hidden="true"></i> Info UNEDI</a>
  						<a class="dropdown-item active" href="../campus.php"><i class="fa fa-home" aria-hidden="true"></i> Campus UNEDI</a>
  						
					 		</div>
					 	</div>
					 </li>
					 </div>
				 </ul>
			 </div>