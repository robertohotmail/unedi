<!DOCTYPE HTML>
<html>
<head>
<title>UNEDI-Sitio-Web</title>
<link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="../css/font-awesome.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary JavaScript plugins) -->
<!-- Custom Theme files -->

<link href="../css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="../application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="../js/jquery.min.js"></script>
 <script src="../js/bootstrap.js"></script>

</head>
<?php
session_start();
if (!isset($_SESSION['id_session_usuario']))
    header("location:index.php");
?>
<!-- banner --> 
<div class="">	  
	 <div class="header">
			 <div class="logo">
				 <a href="home.php"><img src="../images/LOGO UNEDI.png" alt=""/></a>
			 </div>
			<?php include 'navbar.php'?>
			 <!-- script-for-menu -->
		 <script>
				$("span.menu").click(function(){
					$("ul.navig").slideToggle("slow" , function(){
					});
				});
		 </script>
		 <!-- script-for-menu -->
			 <div class="clearfix"></div>
	 </div>	  
</div>
<!---->
<?php

/* Llamar la Cadena de Conexion*/ 
include ("../conexion.php");
$active_config="active";
$active_banner="active";
?>
    
    <div class="container">


    			  	<div>
					  
	</div>
      <!-- Main component for a primary marketing message or call to action -->
      <div class="row">	
			<div class="row">
			  <div class="col-xs-12 text-right">
				  <a href='banneradd.php' class="btn btn-default" ><span class="glyphicon glyphicon-plus"></span> Subir Imagen</a>
			  </div>
			</div>	
		  <br>


		     
		  <div class="outer_div"></div><!-- Datos ajax Final -->
	  </div>
    </div> <!-- /container -->
	<?php include("../footer.php");?>

  </body>
</html>
<script>
	$(document).ready(function(){
		load(1);
	});
	function load(page){
		var parametros = {"action":"ajax","page":page};
		$.ajax({
			url:'./ajax/banner_ajax.php',
			data: parametros,
			 beforeSend: function(objeto){
			$("#loader").html("<img src='../img/ajax-loader.gif'>");
		  },
			success:function(data){
				$(".outer_div").html(data).fadeIn('slow');
				$("#loader").html("");
			}
		})
	}
	function eliminar_slide(id){
		page=1;
		var parametros = {"action":"ajax","page":page,"id":id};
		if(confirm('Esta acción  eliminará de forma permanente el banner \n\n Desea continuar?')){
		$.ajax({
			url:'./ajax/banner_ajax.php',
			data: parametros,
			 beforeSend: function(objeto){
			$("#loader").html("<img src='../images/ajax-loader.gif'>");
		  },
			success:function(data){
				$(".outer_div").html(data).fadeIn('slow');
				$("#loader").html("");
			}
		})
	}
	}
</script>