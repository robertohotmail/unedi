<!---->
<div class="footer">
	 <div class="container">
		 <div class="ftr-sec">
			 <div class="col-md-4 ftr-grid">
				 <h3>Sitio web Unedi</h3>
				 <p>Este sitio fue elaborado por UNIANDES Ext-Ibarra, 2019-2020 por el grupo de vinvulación perteneciente a la ultima generacion de estudiantes de la Carrera de Sistemas coordinados por el Ing. Andrés León.</p>
			 </div>
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<!---->
<div class="copywrite">
	 <div class="container">
		 <p>Copyright © 2019-2020 UNEDI-UNIANDES-EXT-IBARRA. </p>
	 </div>
</div>
<!---->
</body>
</html>