<?php 
include 'header.php';
include 'logo.php';
include 'parameters.php';
include 'login.php';
?>
<body>
<div class="about-sec">
	 <div class="container">
		  <div class="about-head">
		  <b><h2><font color="F4F6F7">ACERCA DE UNEDI</font> </h2></b>
		  </div>
		  <hr/>
		  <div class="video-grid">
			 <div class="col-md-6 video">
				 <img src="images/f122.jpg" width="70%">
			 </div>
			 <div class="col-md-6 video-info">
				 <h3>Misión.</h3>
				 <h4><font color="F4F6F7">Al presente :</font></h4>
				 <p align="justify"><font color="000000"><?php echo utf8_encode($m);?></font></p>
				 <h3>Visión.</h3>
				 <h4><font color="F4F6F7">Al futuro :</font></h4>
				 <p align="justify"><font color="000000"> <?php echo utf8_encode($v);?></font> </p>
			 </div>
			 <div class="clearfix"></div>
		  </div>
		  <div class="our_work">
			 <h3><font color="F4F6F7">Nuestras Instalaciones</font></h3>
			 <hr/>
			 <div class="blog-section">
			 	<?php
                 		$query = $conexion -> query ("SELECT * FROM noticia WHERE eliminado ='n' and categoria='Campus' LIMIT 3");
                  		while ($valores = mysqli_fetch_array($query)) {
  						echo '<div class="col-md-4 camp-grid">
  						<a href="#"><img src="/unedi/uploads/'.$valores['imagen'].'" class="img-responsive" alt=""/></a>
  						<div class="blg-pic-info"> <h4><a href="#">'.$valores['titulo'].'</a></h4><p align="justify">'.$valores['descripcion'].'</p>
						 </div></div>';
                  		}?>
				  <div class="clearfix"></div>				
			 </div>
		 </div>
	</div>
</div>
<!---->
<?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>