<div class="top-menu">
				 <span class="menu"></span>
				 <ul class="navig">				 
		<li class="active"><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> Inicio</a></li>
		<li>
			<div class="dropdown">
				<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-flag" aria-hidden="true"></i> 
					 		Eventos
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					 		<a class="dropdown-item active" href="programa.php"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;Calendario</a>
 							<a class="dropdown-item active" href="noticias.php"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;Noticias</a>
 							<a class="dropdown-item active" href="galeria.php"><i class="fa fa-laptop" aria-hidden="true"></i>&nbsp;Galería</a>
					 		</div>
					 	</div></li>
		<li>
			<div class="dropdown">
				<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-book" aria-hidden="true"></i> 
					 		Sistemas Enlazados
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					 		<a class="dropdown-item active" href="http://www.unedi.edu.ec/blog/wp-login.php?redirect_to=http%3A%2F%2Fwww.unedi.edu.ec%2Fblog"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Acceso al portal Estudiantil</a>
 							<a class="dropdown-item active" href="http://www.unedi.edu.ec/blog/consultar-notas-estudiantes/"><i class="fa fa-desktop" aria-hidden="true"></i>&nbsp; Consultar Notas de Estudiantes</a>
					 		</div>
					 	</div>
					 </li>
		<li>
			<div class="dropdown">
				<a href="#" class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-tasks" aria-hidden="true"></i> 
					 		Acerca de 
					 	</a>
					 	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					 		<a class="dropdown-item active" href="oferta.php"><i class="fa fa-folder" aria-hidden="true"></i>&nbsp;Oferta académica</a>
					 		<a class="dropdown-item active" href="acercade.php"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;Acerca de la UNEDI</a>
							 <a class="dropdown-item active" href="contactos.php"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;Contáctanos</a>
							 <a class="dropdown-item active" href="cuadro.php"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;Cuadro de Honor</a>
 							<a class="dropdown-item active" href="personal.php"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Personal de Unedi</a>
					 		</div>
					 	</div>
					 </li>

			
		<li><a data-toggle="modal" data-target="#modalForm">
    		<i class="fa fa-user-secret" aria-hidden="true"></i> Administración</a></li>
    	</ul>
    	</div>