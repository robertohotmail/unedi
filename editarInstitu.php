<?php
include('conexion.php');
include('logoAdmin.php');
?>
<?php

$title = "Editar cuadro";
/* Llamar la Cadena de Conexion*/


$id = (int) $_GET['id']; 
$sql = mysqli_query($conexion, "select * from informacioninstitucional where id_dato='$id' limit 0,1");
$count = mysqli_num_rows($sql);
if ($count == 0) {
  //header("location: bannerlist.php");
  //exit;
}
$rw = mysqli_fetch_array($sql);
$nombre = $rw['nombreInstitucion'];
$telefono = $rw['telefono'];
$correo = $rw['correoInsitucion'];
$sitioWeb = $rw['sitioWeb'];
$mision = $rw['mision'];
$vision = $rw['vision'];
$estado = $rw['estado'];
$direcionInstitucion = $rw['direccionIntitucion'];
$periodoAcademico = $rw['periodoAcademico'];

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  echo $id;
  $nombre = $_POST['nombreInstitucion'];
  $telefono = $_POST['telefono'];
  $correo = $_POST['correoInsitucion'];
  $sitioWeb = $_POST['sitioWeb'];
  $mision = $_POST['mision'];
  $vision = $_POST['vision'];
  $estado = $_POST['estado'];
  $direcionInstitucion = $_POST['direccionIntitucion'];
  $periodoAcademico = $_POST['periodoAcademico'];

  $query = "UPDATE informacioninstitucional set 	nombreInstitucion='$nombre', telefono='$telefono',correoInsitucion ='$correo', sitioWeb='$sitioWeb',mision='$mision', vision='$vision', direccionIntitucion='$direcionInstitucion', 	periodoAcademico='$periodoAcademico' WHERE id_dato='$id'";
  mysqli_query($conexion, $query);
  echo '<script type="text/javascript">
           window.location = "http://localhost/unedi/adminInstitucion.php"
      </script>';
}

?>
<div class="container">
<div class="blog-head">
        <h2><font color="F4F6F7">Modifica la información de UNEDI</font></h2>
     
			</div>
      </div>

      <div class="container">

  <form action="editarInstitu.php?id=<?php echo $_GET['id']; ?>" method="POST" enctype="multipart/form-data">
    
  
  
<div class="container">
  <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Nombre de la Intitución</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="nombreInstitucion" value="<?php echo $nombre; ?>" required name="nombreInstitucion">
      </div>
    </div>
    </div>

    <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Telefono</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="telfono" value="<?php echo $telefono; ?>" required name="telefono">
      </div>
    </div>
    </div>

    <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Correo</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="correoInsitucion" value="<?php echo $correo; ?>" required name="correoInsitucion">
      </div>
    </div>
    </div>

    <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Sitio Web</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="sitioWeb" value="<?php echo $sitioWeb; ?>" required name="sitioWeb">
      </div>
    </div>
    </div>

    <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Misión</label>
      <div class="col-sm-9">
        <textarea class='form-control' name="mision" id="mision" required rows=6><?php echo $mision; ?></textarea>
      </div>
      </div>
      </div>

      <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Visión</label>
      <div class="col-sm-9">
        <textarea class='form-control' name="vision" id="vision" required rows=6><?php echo $vision; ?></textarea>
      </div>
      </div>
      </div>

      <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Dirección</label>
      <div class="col-sm-9">
        <textarea class='form-control' name="direccionIntitucion" id="direccionIntitucion" required rows=6><?php echo $direcionInstitucion; ?></textarea>
      </div>
      </div>
      </div>

      <div class="container">
    <div class="form-group">
      <label for="titulo" class="col-sm-2 control-label">Periodo Académico</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="periodoAcademico" value="<?php echo $periodoAcademico; ?>" required name="periodoAcademico">
      </div>
    </div>
    </div>

      <div class="container">
      <div class="form-group">
      <div class="col-sm-4">
              
    </div>
    </div>

    <div class="container">
    <div class="form-group">
      <div id='loader'></div>
      <div class='outer_div'></div>
      <div class=" col-sm-7">
        <button type="submit" class="btn btn-success" name="update">Actualizar datos</button>
      </div>
    </div>
    </div>
  </form>



  </div>

 