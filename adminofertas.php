<?php
include 'header.php';
include 'conexion.php';
include 'parameters.php';
require('conexion.php');
session_start();
if(isset($_SESSION["usuario"])){
  header("location: index.php");
}
?>
 <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
  <script src="js/responsiveslides.min.js"></script>
<script> 
    $(function () {
      $("#slider").responsiveSlides({
        auto: true,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
  </script>  
<div class="banner">    
   <div class="header">
       <div class="logo">
         <a href="home.php"><img src="images/LOGO.png" alt=""/></a>
       </div>
       <?php include 'navbaradmin.php'?>
        <!-- script-for-menu -->
     <script>
        $("span.menu").click(function(){
          $("ul.navig").slideToggle("slow" , function(){
          });
        });
     </script>
   </div>
     <!-- ESTA SECCIONN ES LA DEL SLIDER EN MOVIMIENTO--> 
     </div>
    <br><br><br><br><br><br>


<div class="container">
<!-- Boton para accionar modal -->
<center>
<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalForm">
    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <h3 >Agregar Oferta</h3>
</button>
</center> <br><br><br>
    <table class="table table-bordered"  id="tablaDatos">
        <thead>
            <tr>
                <th class="text-center">Nro</th>
                <th>Titulo</th>
                <th>Descripción</th>
                <th>Fecha</th>
                <th>Imagen</th>
                <th class="text-center">Opciones</th>
            </tr>
        </thead>
        <tbody      >
          <?php
            $consulta = "SELECT * from oferta where eliminado ='n'  <> 'Campus'";
            $resultado = mysqli_query($conexion, $consulta);
                while ($row = mysqli_fetch_array($resultado)) {
                  $id = $row[0];
                  echo "
                  <tr>
                    <td>".$row[0]."</td>
                    <td>".$row[1]."</td>
                    <td>".$row[2]."</td>
                    <td>".$row[3]."</td>
                    <td><img src='uploads/".$row[4]."' width='25%'></td>
                  
                   
                    <td>";
                    echo '
                     <a class="btn btn-warning" href="editarOferta.php?id=' . $id . '">Editar 
                    </a> <a class="btn btn-danger" href="elimOferta.php?id=' . $id . '">Eliminar</a></td>
                  </tr>';
          }
          ?>
    </table>
</div>   
<!-- Modal del formulario --> 
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <!-- Header del Modal del formulario --> 
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Agregar Oferta</h4>
            </div>
            <!-- Cuerpo del Modal del formulario -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" action="addOferta.php" enctype="multipart/form-data" method="post">
               <div class="form-group">
                <label class="form-control text-center" for="txtTitulo">Titulo:</label>
               <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Ej. Nueva Oferta"><br>
             </div>
              <div class="form-group">
              <label class="form-control  text-center" for="txtDescripcion">Contenido de la oferta:</label>
              <textarea class="form-control" type="text" name="txtDescripcion" id="txtDescripcion" placeholder="Ej. La oferta se trata de este tema"></textarea><br>
              </div>
              <div class="form-group">
                <label class="form-control text-center" for="txtFoto">Fotografia:</label>
               <input type="file" class="form-control" name="txtFoto" id="txtFoto" ><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtFecha">Fecha:</label>
               <input type="text" class="form-control" readonly="" name="txtFecha" id="txtFecha" value="<?php $x = date("d/m/Y"); print_r($x);?>"><br>
             </div>
            
            <input class="form-control btn btn-primary" type="submit" value="Guardar">
            </form>
          </div>
          </div>
        </div>
      </div>
      <?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>
