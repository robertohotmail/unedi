<?php
include('conexion.php');
include('logoAdmin.php');
?>
<body>
<div class="container">
<!-- Boton para accionar modal -->
<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalForm">
    <i class="fa fa-home" aria-hidden="true"></i> <h3 >Info del  campus</h3>
</button> <br><br><br>
    <table class="table table-bordered"  id="tablaDatos">
        <thead>
            <tr>
                <th>Lugar</th>
                <th>Descripción</th>
                <th>Imagen</th>
                <th>Estado</th>
                <th class="text-center">Opciones</th>
            </tr>
        </thead>
        <tbody      >
            <?php
            $consulta = "SELECT * from noticia where categoria ='Campus' AND eliminado='n'";
            $resultado = mysqli_query($conexion, $consulta);
                while ($row = mysqli_fetch_array($resultado)) {
                  if($row[5]="a"){
                    $e = "<div class='alert-success'>Activado</div>";
                  }else{
                    $e = "<div class='alert-danger'>Desactivado</div>";
                  }
                  echo "
                  <tr>
                    <td>".$row[1]."</td>
                    <td>".$row[2]."</td>
                    <td><img src='uploads/".$row[4]."' width='25%'></td>
                    <td>".$e."</td>
                    <td>
                    <a class='btn btn-danger' href='editarNoticia.php?id='".$row['id_noticia']."'>Actualizar</a>
                    <a class='btn btn-warning' href='elimNoticia.php?id='".$row['id_noticia']."'>Eliminar</a></td>
                  </tr>";
          }
          ?>
    </table>
</div>   
<!-- Modal del formulario -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <!-- Header del Modal del formulario --> 
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-home" aria-hidden="true"></i> Agregar nueva parte del campus</h4>
            </div>
            <!-- Cuerpo del Modal del formulario -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" action="addCampus.php" enctype="multipart/form-data" method="post">
               <div class="form-group">
                <label class="form-control text-center" for="txtTitulo">Nombre:</label>
               <input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="Ej. Sala de Maestros"><br>
             </div>
              <div class="form-group">
              <label class="form-control  text-center" for="txtDescripcion">Contenido del campus:</label>
              <textarea class="form-control" type="text" name="txtDescripcion" id="txtDescripcion" placeholder="Ej. Descripcion del campus"></textarea><br>
              </div>
              <div class="form-group">
                <label class="form-control text-center" for="txtFoto">Fotografia:</label>
               <input type="file" class="form-control" name="txtFoto" id="txtFoto" ><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtFecha">Fecha de Registro:</label>
               <input type="text" class="form-control" readonly="" name="txtFecha" id="txtFecha" value="<?php $x = date("d/m/Y"); print_r($x);?>"><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtCategoria">Categoría:</label>
                <input type="text" class="form-control" name="txtCategoria" id="txtCategoria" placeholder="Ej. Campus" value="Campus" readonly=""><br>
             </div>
            <input class="form-control btn btn-primary" type="submit" value="Guardar">
            
            </form>
          </div>
            
          </div>
        </div>
      </div>
