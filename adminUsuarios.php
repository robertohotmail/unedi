<?php
include('conexion.php');
include('logoAdmin.php');
?>
<body>
  
<div class="container">
<!-- Boton para accionar modal -->
<center>
<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalForm">
    <i class="fa fa-user" aria-hidden="true"></i> Agregar Usuario
</button>
</center> <br><br><br>
    <table class="table table-bordered"  id="tablaDatos">
        <thead>
            <tr>
                <th class="text-center">Nro</th>
                <th>Nombre de Usuario</th>
                <th>Contraseña</th>
                <th>Categoría</th>
                <th>Estado</th>
                <th class="text-center">Opciones</th>
            </tr>
        </thead>
        <tbody      >
            <?php
            $consulta = "SELECT * from usuario where eliminado ='n'";
            $resultado = mysqli_query($conexion, $consulta);
                while ($row = mysqli_fetch_array($resultado)) {
                  $id=$row[0];
                  echo "
                  <tr>
                    <td>".$id."</td>
                    <td>".$row[1]."</td>
                    <td>".$row[2]."</td>
                    <td>".$row[3]."</td>
                    <td>".$row[4]."</td>
                    <td>
                    ";
                    echo '
                    <a class="btn btn-warning" href="editarPassUsuario.php?id='.$id.'">Actualizar
                    </a>
                    <a class="btn btn-danger" href="elimarUsuario.php?id='.$id.'">Eliminar</a></td>
                  </tr>';

          }
          ?>
    </table>
</div>   
<!-- Modal del formulario -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <!-- Header del Modal del formulario --> 
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <center>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user" aria-hidden="true"></i> Agregar Usuario</h4>
                </center>
            </div>
            <!-- Cuerpo del Modal del formulario -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" action="addUsuario.php" enctype="multipart/form-data" method="post">
               <div class="form-group">
                <label class="form-control text-center" for="txtUsuario">Nombre de Usuario:</label>
               <input type="text" class="form-control" name="txtUsuario" id="txtUsuario" placeholder="Ej. juanIntriago" required=""><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtPassword">Password:</label>
               <input type="password" class="form-control" name="txtPassword" id="txtPassword" required=""><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtCategoria">Tipo de Usuario:</label>
               <select class="form-control" name="txtCategoria" id="txtCategoria" required="">
                <option>--Seleccione su Tipo de --</option>
                 <?php
                 $query = $conexion -> query ("SELECT * FROM categoriausuario");
                    
                  while ($valores = mysqli_fetch_array($query)) {
                        
                  echo '<option value="'.$valores[descripcion].'">'.$valores[descripcion].'</option>';
                  }?>
                  <option>--Fin de la Lista--</option>
               </select> <br>
             </div>
            <input class="form-control btn btn-primary" type="submit" value="Guardar">
            
            </form>
          </div>
            
          </div>
        </div>
      </div>
      <?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>
