<?php 
include 'header.php';
include 'logo.php';
include 'parameters.php';
include 'login.php';
?>
<!--contact-->
<div class="contact">
	 <div class="container">
		 <div class="main-head-section">
					<h2><font color="F4F6F7">Contactos </font></h2>
			<div class="contact-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.7536429750676!2d-78.12067348590548!3d0.32624046410125684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e2a3df674c001e7%3A0x362ba9e000d908b!2sUnedi!5e0!3m2!1ses-419!2sec!4v1583184123488!5m2!1ses-419!2sec" width="650" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		 </div>
		 <div class="contact_top">			 		
			 <div class="col-md-8 contact_left">
			 		<h4>Contactanos-UNEDI</h4>
			 		<p><font color="F4F6F7">En este formulario, se tiene como objetivo que el solicitante(Estudiante, Docente, Padre de Familia) se deje una solicitud de contacto y la razon del mismo, para que se le pueda responder de manera personalizada.</font></p>
					<form>
					  <div class="form_details">
					      <input type="text" class="text" name="txtSolicitante" placeholder="Nombre de Solicitante : - Ej. José Pérez">
						  <input type="text" class="text" name="txtEmail" placeholder="E-mail: Ej. joseperez1985@gmail.com" >
						  <input type="text" class="text" name="txtMotivo" placeholder="Motivo: Ej. Solocitud de Información">
						  <textarea name="txtMensaje">Saludos este es mi mensaje</textarea>
						  <div class="clearfix"> </div>
						 <div class="sub-button">
							 <input type="submit" value="Enviar Mensaje">
						 </div>
					  </div>
				   </form>
			 </div>
			 <div class="col-md-4 company-right">
				 <div class="company_ad">
						<h3>Dirección</h3>
						<span><?php echo $dir;?></span>
			      	 <address>
						 <p><font color="F4F6F7">E-mail : <?php echo $correo;?> </font></p>
						 <p><font color="F4F6F7">Telf: : <?php echo $telef;?></font></p>
						 <p><font color="F4F6F7">Sitio Web: <?php echo $sitioW;?></font></p>
						 <p><font color="F4F6F7">Periodo Lectivo: <?php echo $periodo;?></font></p>									 	 	
					 </address>
				 </div>
			 </div>	
				<div class="clearfix"> </div>
		  </div>
	 </div>
</div>
<?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>