<?php 
include 'header.php';
include 'conexion.php';
include 'logo.php';
include 'login.php';
?>
<!-- blog-page -->
<div class="blog">
		<div class="container">
			<div class="blog-head">
				<center><h2><b><font color="F4F6F7">Cuadro de honor de estudiantes de UNEDI</font> </b></h2></center> </p>
			</div>
			<div class="col-md-8 blog-left">
				<?php
                 		$query = $conexion -> query ("SELECT * FROM cuadrohonor WHERE eliminado ='n' <>'Campus' LIMIT 5");
                  		while ($valores = mysqli_fetch_array($query)) {
  						echo '<div class="blog-info">
                          <h2 <b class="text-dark">'.$valores['nombres'].'</b></h2>
                          <h3 <b class="text-dark">'.$valores['apellidos'].'</b></h3>
                          <h3  class="text-warning">'.$valores['dignidad'].'</h3>
  						<div class="blog-info-text">
  						<div class="blog-img">
							<a href="#"> <img src="uploads/'.$valores['imagen'].'" class="img-responsive zoom-img" alt=""/></a>
						</div>
                        <h4 <b class="text-warning">'.$valores['descripcion'].'</b></h4>
                        <h4 <b class="text-dark">'.$valores['promedio'].'</b></h4>
						</div>
						</div><br>
						<hr width=750>
  						';
                  		}?>
                  			
                  		</div>	
			<!--<div class="col-md-4 single-page-right">
				<div class="category blog-ctgry">
					<h4>Categorias</h4>
					<div class="list-group">
						<?php
            //$query = $conexion -> query ("SELECT * FROM categorianoticia WHERE eliminado ='n'");
                  		//while ($valores = mysqli_fetch_array($query)) {
  						//echo '
  						//<a href="#" class="list-group-item">'.$valores['descripcion'].'</a>
  						//';
                  		//}?>
					</div>
				</div>			
			</div>-->
			<div class="clearfix"> </div>
		</div>	
	</div>	
	<!--//blog-->
<?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>