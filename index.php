<?php 
include 'header.php';
include 'conexion.php';
include 'parameters.php';
require('conexion.php');
session_start();
if(isset($_SESSION["usuario"])){
	header("location: home.php");
}
if(!empty($_POST)){
	$usuario = mysqli_real_escape_string($conexion,$_POST['txtUsuario']);
	$password = mysqli_real_escape_string($conexion,$_POST['txtPassword']);
	$sha1_pass = sha1($password);
	$consulta = "SELECT id_usuario, categoriaUsuario FROM usuario WHERE nombreUsuario = '$usuario' AND password ='$sha1_pass'";
	$result=$conexion->query($consulta);
	$filas = $result->num_rows;

	if($filas > 0){
		$filas = $result->fetch_assoc();
		$_SESSION['id_session_usuario'] = $filas['id_usuario'];
		$_SESSION['usuario'] = $filas['nombreUsuario'];
		$_SESSION['tipo_usuario'] = $filas['categoriaUsuario'];
		header("location: home.php");
	}else{
		$error = "Los datos son incorrectos, intente verificando lo escrito.!";
	}
}
?>
 <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
<body>
<!-- banner -->
<script src="js/responsiveslides.min.js"></script>
<script> 
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	nav: true,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
  </script>  
<div class="banner">	  
	 <div class="header">
			 <div class="logo">
		
				 <a href="index.php"><img src="images/LOGO.png" alt=""/></a>
			 </div>
			 <?php include 'navbar.php'?>
			  <!-- script-for-menu -->
		 <script>
				$("span.menu").click(function(){
					$("ul.navig").slideToggle("slow" , function(){
					});
				});
		 </script>
	 </div>
		 <!-- ESTA SECCIONN ES LA DEL SLIDER EN MOVIMIENTO--> 
		 </div>
		 <br><br><br><br><br><br><br><br><br><br>
		 <div>
		  <tr/>
		  <center><iframe src="carousel.php"  style="height:300px;width:720px;"></iframe></center>
		</div>
		<br><br><br><br><br><br>
		 <!-- seccion de noticias en index-->
		  <div class="container">
			 <div class="col-md-4 banner-grid">
				 <h3>Últimas Noticias</h3>
				 <div class="banner-grid-sec">
					 <div class="grid_info">
					 	<?php
                 		$query = $conexion -> query ("SELECT * FROM noticia WHERE eliminado ='n'  LIMIT 6");
                  		while ($valores = mysqli_fetch_array($query)) {
  						echo '<div class="blg-pic"><img src="/unedi/uploads/'.$valores['imagen'].'" class="img-responsive" alt=""></div>
  						<div class="blg-pic-info"> <h4><a href="#">'.$valores['titulo'].'</a></h4><p>'.$valores['descripcion'].'</p>
						 </div>';
                  		}?>
						 <div class="clearfix"></div>
					 </div> 
					 <div class="grid_info">						 
						 <div class="clearfix"></div>
					 </div>
				 </div>
			 </div>
			 <div class="col-md-4 banner-grid">
				 <h3>Eventos Académicos</h3>
				 <div class="banner-grid-sec">
					 <?php
                 		$querye = $conexion -> query ("SELECT * FROM events WHERE eliminado ='n' LIMIT 6");
                  		while ($valorese = mysqli_fetch_array($querye)) {
  						echo '<div class="news-grid">
						 <h4><a href="#">'.$valorese['title'].'</h4>
					 
  						<p>Este evento se realiza desde : '.$valorese['start'].'  hasta : '.$valorese['end'].'</p></div><br>';
                  		}?>
				 </div>
			 </div>
			 <div class="col-md-4 banner-grid">
				 <h3>Solicitar información</h3>
				 <div class="banner-grid-sec news_sec">
					 <div class="news-ltr">
						 <h4><a href="#">Te enviaremos un email respondiendo a tu solicitud</a></h4>
						 <p>Digite su mail aqui</p>
					 </div>
					 <form>
						 <input type="text" placeholder="Email Address*" required="">
						 <input type="submit" value="Enviar">
					 </form>
				 </div>
			 </div>
			 
			 <div class="clearfix"></div>
		  </div>

		  
		  <div class="welcome">
	 <div class="container">
		 <h1> <font color="#58ACF2"><b><?php echo $nombre;?></b></font></h1>
		 <div class="welcm_sec">
			 <div class="col-md-9 campus">
				 <div class="campus_head">
					 <font color="white"><h4 align="justify"> 
En este sitio puede conocer todo referente a la institución UNEDI PCEI, podrá enterarse de nuestra agenda académica, noticias, ver nuestras fotografías, eventos y muchos más...!
La Unidad Educativa Imbabura para personas con escolaridad inconclusa modadlidad semipresencial, pertenece al Subsistema de Educación Fiscomisional Semipresencial del Ecuador «Monseñor Leonidas Proaño». Al igual que las unidades de cada una de las provincias de la Costa, Sierra y Oriente, todas legalmente reconocidas por el Ministerio de Educación Intercultural mediante Acuerdo Ministerial de creación No.- 1544 del 29 de Octubre de 1991. El 28 de enero del 2015 el Ministerio de Educación, Conferencia Episcopal Ecuatoriana y la Confedec firman un nuevo acuerdo para que se continúe brindando educación a las personas que no han podido continuar estudiando en las instituciones presenciales.

MONSEÑOR LEONIDAS PROAÑO (patrono de la UNEDI)

El 29 de enero de 1910 nace Monseñor Leonidas Eduardo Proaño Villalba quien luego de toda una vida entregada sin reservas a la liberación de los oprimidos se convirtiera en el Profeta de Amerindia, el Pastor de los pobres, el Obispo de los Indios. San Antonio de Ibarra, Provincia de Imbabura, fue su lugar natal.

Por los invalorables aportes eclesiológicos y teológicos se lo considera Padre de la Iglesia Latinoamericana y Padre de la Teología de la Liberación en América Latina, a pesar de que  no fue un teólogo de oficio. Con su transparente vida evangélica mostró a creyentes y no creyentes que el camino de la resurrección pasa por la adhesión y auténtica fe en Jesucristo Liberador y una sincera práctica de la justicia social a favor de los más pobres entre los pobres.

Educador nato, con una pedagogía liberadora, basada en la el análisis de la realidad económica y social a la luz del Evangelio y con gran respeto por las culturas indígenas, logró que el indígena recobrara su dignidad, su identidad y se organizara para su liberación.  Mons. Proaño está en la raíz del gran árbol que constituye el movimiento indígena.  Fue maestro, poeta, historiador, defensor de los derechos humanos y de los derechos de los pueblos indígenas, periodista, internacionalista, “una montaña que orienta y desafía”, “un Chimborazo espiritual”.

El 31 de agosto de 1988 muere en Quito luego de afrontar una dolorosa enfermedad y lenta agonía una voz de la Iglesia comunicaba así el suceso a un Ecuador conmovido, especialmente al Ecuador indígena. Luego de funerales en Riobamba e Ibarra, es enterrado su cadáver en Pucahuaico, San Antonio de Ibarra, a los pies del majestuoso TAITA IMBABURA, regresando al seno de la PACHA MAMA, a sentir la brisa del viento que curtió su piel para hacerse indio entre los indios.</font></h4>
				 </div>				 
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>

<!---->


<!---->
<!-- Modal del formulario -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <!-- Header del Modal del formulario --> 
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <h3 class="modal-title" id="myModalLabel"><i class="fa fa-terminal" aria-hidden="true"></i> Ingresar como Administrador</h3>
            </div>
            <!-- Cuerpo del Modal del formulario -->
            <div class="modal-body">            	
                <p class="statusMsg"></p>
                <form role="form" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
                	<center>
                		<img class="img-responsive pic-center" src="img/unedi.png" width="50%">
                	</center>                	
               <div class="form-group">
                <label class="form-control text-center" for="txtUsuario">Usuario:</label>
               <input type="text" class="form-control" name="txtUsuario" id="txtUsuario" placeholder="Ej. consueloHernandez"><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtPassword">Password:</label>
                <input type="password" class="form-control" name="txtPassword" id="txtPassword"placeholder="Digite aqui su password y no la olvide"><br>
             </div>
            <input class="form-control btn btn-primary" type="submit" value="Acceder">            
            </form>
          </div>            
          </div>
        </div>
      </div>
<?php 
include 'footer.php';
?>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/main.js"></script>
    </body>