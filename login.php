<?php
if(!empty($_POST)){
	$usuario = mysqli_real_escape_string($conexion,$_POST['txtUsuario']);
	$password = mysqli_real_escape_string($conexion,$_POST['txtPassword']);

	$sha1_pass = sha1($password);

	$consulta = "SELECT id_usuario, categoriaUsuario FROM usuario WHERE nombreUsuario = '$usuario' AND password ='$sha1_pass'";
	$result=$conexion->query($consulta);
	$filas = $result->num_rows;

	if($filas > 0){
		$filas = $result->fetch_assoc();
		$_SESSION['id_session_usuario'] = $filas['id_usuario'];
		$_SESSION['usuario'] = $filas['nombreUsuario'];
		$_SESSION['tipo_usuario'] = $filas['categoriaUsuario'];

		header("location: home.php");

	}else{
		$error = "Los datos son incorrectos, intente verificando lo escrito.!";
	}
}
?>
<!-- Modal del formulario -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <!-- Header del Modal del formulario --> 
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Cerrar</span>
                </button>
                <h3 class="modal-title" id="myModalLabel"><i class="fa fa-terminal" aria-hidden="true"></i> Ingresar como Administrador</h3>
            </div>
            <!-- Cuerpo del Modal del formulario -->
            <div class="modal-body">            	
                <p class="statusMsg"></p>
                <form role="form" action="index.php" method="post">
                	<center>
                		<img class="img-responsive pic-center" src="img/unedi.png" width="50%">
                	</center>                	
               <div class="form-group">
                <label class="form-control text-center" for="txtUsuario">Usuario:</label>
               <input type="text" class="form-control" name="txtUsuario" id="txtUsuario" placeholder="Ej. consueloHernandez"><br>
             </div>
             <div class="form-group">
                <label class="form-control text-center" for="txtPassword">Password:</label>
                <input type="password" class="form-control" name="txtPassword" id="txtPassword"placeholder="Digite aqui su password y no la olvide"><br>
             </div>
            <input class="form-control btn btn-primary" type="submit" value="Acceder">            
            </form>
          </div>            
          </div>
        </div>
      </div>